
#ifndef _TETHER_FIELD_H
#define _TETHER_FIELD_H

#include <maya/MPxFieldNode.h>
#include <maya/MTypeId.h>

class tetherField : public MPxFieldNode
{
public:
	tetherField();
	virtual ~tetherField();

	virtual MStatus compute(const MPlug &plug, MDataBlock &data);
	virtual MStatus getForceAtPoint(const MVectorArray &point,
									const MVectorArray &velocity,
									const MDoubleArray &mass,
									MVectorArray &force,
									double deltaTime);


	static void *creator();
	static MStatus initialize();
	static MTypeId id;

	static MObject aDiskPosition;
	static MObject aDiskNormal;
	static MObject aRadialMagnitude;
	static MObject aOrbitalMagnitude;
	static MObject aRadialMagnitudeScale;
	static MObject aOrbitalMagnitudeScale;

	// static MObject aConstraintPlaneNormal;

	static MObject aRadialMagnitudeFalloff;
	static MObject aOrbitalMagnitudeFalloff;

private:

	MStatus calculateForce(
		MDataBlock &data,
		const MVectorArray &points,
		const MVectorArray &velocities,
		const MDoubleArray &masses,
		const double &dt,
		MVectorArray &forces);
};

#endif
