

#include <maya/MIOStream.h>
#include <math.h>


#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MTime.h> 
#include <maya/MArrayDataBuilder.h>
#include <maya/MRampAttribute.h>
#include "errorMacros.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "tether_field.h"

MObject     tetherField::aDiskPosition;	
MObject     tetherField::aDiskNormal;
MObject     tetherField::aRadialMagnitude;
MObject     tetherField::aOrbitalMagnitude;

// MObject     tetherField::aConstraintPlaneNormal;
MObject     tetherField::aRadialMagnitudeFalloff;
MObject     tetherField::aOrbitalMagnitudeFalloff;

MObject     tetherField::aRadialMagnitudeScale;
MObject     tetherField::aOrbitalMagnitudeScale;

// MObject     tetherField::aUseFalloffRamps;

MTypeId tetherField::id( k_tetherField );

tetherField::tetherField(){
}
tetherField::~tetherField(){
}

void *tetherField::creator()
{
	return new tetherField;
}

MStatus tetherField::initialize()
{
	MStatus st;


	MFnNumericAttribute 	nAttr;
	MFnTypedAttribute 		tAttr;
	// MFnEnumAttribute 		eAttr;

	aDiskPosition = tAttr.create("diskPosition", "dpos",MFnData::kVectorArray);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aDiskPosition); mser;

	aDiskNormal = tAttr.create("diskNormal", "dnm",MFnData::kVectorArray);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aDiskNormal); mser;

	aRadialMagnitude = tAttr.create("radialMagnitude", "rmag",MFnData::kDoubleArray);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aRadialMagnitude); mser;

	aOrbitalMagnitude = tAttr.create("orbitalMagnitude", "omag",MFnData::kDoubleArray);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aOrbitalMagnitude); mser;


	aRadialMagnitudeScale = nAttr.create("radialMagnitudeScale", "rms", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aRadialMagnitudeScale); mser;	

	aOrbitalMagnitudeScale = nAttr.create("orbitalMagnitudeScale", "oms", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aOrbitalMagnitudeScale); mser;	


    // aConstraintPlaneNormal = nAttr.createPoint("constraintPlaneNormal", "cpn");
    // nAttr.setStorable(true);
    // nAttr.setReadable(true);
    // nAttr.setKeyable(true);
    // addAttribute(aConstraintPlaneNormal);

 	aRadialMagnitudeFalloff = MRampAttribute::createCurveRamp("radialMagnitudeFalloff", "rmf");
	st = addAttribute(aRadialMagnitudeFalloff);
	mser;

	aOrbitalMagnitudeFalloff = MRampAttribute::createCurveRamp("orbitalMagnitudeFalloff", "omf");
	st = addAttribute(aOrbitalMagnitudeFalloff);
	mser;


	return( MS::kSuccess );
}


MStatus tetherField::compute(const MPlug& plug, MDataBlock& data)
{

	MStatus st;

	if( !(plug == mOutputForce) ) return( MS::kUnknownParameter );

	MVectorArray outForce;


	MString nodeName = MFnDependencyNode(thisMObject()).name();



	/////////////////////////	/////////////////////////

	int multiIndex = plug.logicalIndex( &st ); msert;

	short int nodeState = data.inputValue( state).asShort();
	if (nodeState == 0)  { // normal

		if (st.error()) return( MS::kFailure);

		MArrayDataHandle hInputArray = data.outputArrayValue( mInputData, &st );msert;

		st = hInputArray.jumpToElement( multiIndex );msert;

		MDataHandle hCompound = hInputArray.inputValue( &st );mser;

		MDataHandle hPosition = hCompound.child( mInputPositions );
		MObject dPosition = hPosition.data();
		MFnVectorArrayData fnPosition( dPosition );
		MVectorArray points = fnPosition.array( &st );mser;

		// MDataHandle hVelocity = hCompound.child( mInputVelocities );
		// MObject dVelocity = hVelocity.data();
		// MFnVectorArrayData fnVelocity( dVelocity );
		// MVectorArray velocities = fnVelocity.array( &st );mser;

		// MDataHandle hMass = hCompound.child( mInputMass );
		// MObject dMass = hMass.data();
		// MFnDoubleArrayData fnMass( dMass );
		// MDoubleArray mass = fnMass.array( &st);mser

		// double dt= 0.0;
		// MDataHandle dtH = hCompound.child( mDeltaTime );
		// MTime dT = dtH.asTime();
		// dt = dT.as( MTime::kSeconds );

		unsigned len = points.length();
		outForce=MVectorArray(len, MVector::zero);
		st = calculateForce(data,points,velocities,mass,dt,outForce);
	}

	MArrayDataHandle hOutArray = data.outputArrayValue( mOutputForce, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st ); mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnVectorArrayData fnOutput;
	MObject dOutput = fnOutput.create(outForce , &st );mser;

	hOut.set( dOutput);
	data.setClean( plug );
	return( MS::kSuccess );
}



MStatus tetherField::calculateForce(
	MDataBlock &data,
	const MVectorArray &points,
	const MVectorArray &velocities,
	const MDoubleArray &masses,
	const double & dt,
	MVectorArray &forces
){
	MStatus st = MS::kSuccess;

	const double bigNum = 10e+37 ;
	unsigned pl = points.length();

	MDataHandle hDiskPosition = data.inputValue(aDiskPosition);
	MObject dDiskPosition = hDiskPosition.data();
	MVectorArray diskPosition = MFnVectorArrayData(dDiskPosition).array();
	if (pl != diskPosition.length()) {
		MGlobal::displayWarning("Tether field: diskPosition array must be the same length as the points array.");
		
		return MS::kUnknownParameter;
	}

	MDataHandle hDiskNormal = data.inputValue(aDiskNormal);
	MObject dDiskNormal = hDiskNormal.data();
	MVectorArray diskNormal = MFnVectorArrayData(dDiskNormal).array();
	if (pl != diskNormal.length()) {
		MGlobal::displayWarning("Tether field: diskNormal array must be the same length as the points array.");
		cerr <<"pl:"<< pl<< " diskNormal.length():"<< diskNormal.length() << endl;
		return MS::kUnknownParameter;
	}
	MDataHandle hRadialMagnitude = data.inputValue(aRadialMagnitude);
	MObject dRadialMagnitude = hRadialMagnitude.data();
	MDoubleArray radialMagnitude = MFnDoubleArrayData(dRadialMagnitude).array();
	if (pl != radialMagnitude.length())  {
		MGlobal::displayWarning("Tether field: radialMagnitude array must be the same length as the points array.");
		cerr <<"pl:"<< pl<< "radialMagnitude.length():"<< radialMagnitude.length() << endl;
		return MS::kUnknownParameter;
	}
	MDataHandle hOrbitalMagnitude = data.inputValue(aOrbitalMagnitude);
	MObject dOrbitalMagnitude = hOrbitalMagnitude.data();
	MDoubleArray orbitalMagnitude = MFnDoubleArrayData(dOrbitalMagnitude).array();
	if (pl != orbitalMagnitude.length())  {
		MGlobal::displayWarning("Tether field: orbitalMagnitude array must be the same length as the points array.");
		cerr <<"pl:"<< pl<< "orbitalMagnitude.length():"<< orbitalMagnitude.length() << endl;
		return MS::kUnknownParameter;
	}
    MObject thisObj = thisMObject();
    MRampAttribute radialRamp( thisObj , aRadialMagnitudeFalloff, &st ); mser;
	MRampAttribute orbitalRamp( thisObj , aOrbitalMagnitudeFalloff, &st ); mser;

	forces.setLength(pl);

	double magnitude = data.inputValue(mMagnitude).asDouble();
	float  maxDist = float(data.inputValue( mMaxDistance ).asDouble());

	double radialScale	=	data.inputValue(aRadialMagnitudeScale).asDouble();
	double orbitalScale	=	data.inputValue(aOrbitalMagnitudeScale).asDouble();
	

	float radialAtten;
	float orbitalAtten;
	for (unsigned i = 0; i<pl; i++) {
		MVector diff = points[i] - diskPosition[i];
		MVector diffN = diff.normal();
		MVector radialForce = diffN * radialMagnitude[i];
		MVector orbitalForce = (diffN^diskNormal[i]).normal() * orbitalMagnitude[i];
		float dist = diff.length();
		if (dist < maxDist) {
			dist = dist / maxDist;
			radialRamp.getValueAtPosition( dist, radialAtten, &st ); mser;
			orbitalRamp.getValueAtPosition( dist, orbitalAtten, &st ); mser;
			radialForce *=radialAtten * radialScale;
			orbitalForce *=orbitalAtten * orbitalScale;
		}

		MVector & f = forces[i];
		f = (orbitalForce+radialForce)*magnitude;
	}
 

	return( MS::kSuccess );
}


MStatus tetherField::getForceAtPoint(const MVectorArray&	points,
	const MVectorArray&	velocities,
	const MDoubleArray&	masses,
	MVectorArray&	forceArray,
	double	deltaTime)
{
	MDataBlock data = forceCache();
	calculateForce(data, points, velocities, masses, deltaTime ,forceArray);
	return MS::kSuccess;
}
