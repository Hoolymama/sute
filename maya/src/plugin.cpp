#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"

// #include "geometry_field.h"

// #include "point_field.h"

#include "tether_field.h"


MStatus initializePlugin(MObject obj)
{

	MStatus st;

	MString method("initializePlugin");

	MFnPlugin plugin(obj, PLUGIN_VENDOR, PLUGIN_VERSION, MAYA_VERSION);

	// need kd tree (kindle) loaded before this plugin
	// MGlobal::executePythonCommand("import pymel.core;pymel.core.loadPlugin('Kindle')");

	// st = plugin.registerNode(
	// 	"geometryField",
	// 	geometryField::id,
	// 	geometryField::creator,
	// 	geometryField::initialize,
	// 	MPxNode::kFieldNode);
	// mser;

	// st = plugin.registerNode("pointField", pointField::id, pointField::creator, pointField::initialize, MPxNode::kFieldNode);


	st = plugin.registerNode("tetherField", tetherField::id, tetherField::creator, tetherField::initialize, MPxNode::kFieldNode);
	
	mser;

	MGlobal::executePythonCommandOnIdle("from sute import menu;menu.SuteMenu()", true);

	return st;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin(obj);
	st = plugin.deregisterNode(tetherField::id);
	mser;

	// st = plugin.deregisterNode(pointField::id);
	// mser;

	// st = plugin.deregisterNode(geometryField::id);
	// mser;

	return st;
}
