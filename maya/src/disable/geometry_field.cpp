

#include <maya/MIOStream.h>
#include <math.h>


#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MPlugArray.h> 
#include <maya/MTime.h> 


#include <maya/MArrayDataBuilder.h>


#include "ptKdTree.h"
#include "triKdTree.h"
// #include "triTreeNode.h"
#include "triTreeData.h"


#include "errorMacros.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "geometry_field.h"


MObject     geometryField::aTriTree;	
MObject     geometryField::aBothNormals;
MObject     geometryField::aSides;
MObject     geometryField::aNormal;			
MObject     geometryField::aDrag;		
MObject     geometryField::aRadialDrag;	
MObject     geometryField::aEasyway;	

MObject     geometryField::aNormalLookup;				
MObject     geometryField::aDragLookup;		
MObject     geometryField::aRadialDragLookup;	
MObject     geometryField::aEasywayLookup;	

MObject     geometryField::aCollision;		
MObject     geometryField::aOffset;		
MObject     geometryField::aBounce;
MObject     geometryField::aFriction;
MObject     geometryField::aNormalsMethod;
MObject     geometryField::aNormalFalloff;

MTypeId geometryField::id( k_geometryField );

geometryField::geometryField(){
}
geometryField::~geometryField(){
}

void *geometryField::creator()
{
	return new geometryField;
}


MStatus geometryField::getLookup(const MObject &node, MObject &attr, lookup & result){
	MStatus st;
	MFnAnimCurve aniFn; 
	st = getAniCurveFn(node, attr, aniFn);
	if (!(st.error())) 	result.create(aniFn, 20);
	return st;
}
	

MStatus geometryField::initialize()
{
	MStatus st;


	MFnNumericAttribute 	nAttr;
	MFnTypedAttribute 		tAttr;
	MFnEnumAttribute 		eAttr;

	aTriTree = tAttr.create("triTreeNode", "trt", triTreeData::id ) ; 
	tAttr.setStorable(false);
	tAttr.setCached(false);	
	st = addAttribute( aTriTree ); mser;

	aSides = eAttr.create("sides", "sds");
	eAttr.addField("off", kSidesOff);
	eAttr.addField("top", kSidesTop);
	eAttr.addField("bottom", kSidesBottom);
	eAttr.addField("both", kSidesBoth);
	eAttr.setDefault(kSidesBoth);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aSides );mser;

	aNormalsMethod = eAttr.create("normalsMethod", "nmth");
	eAttr.addField("surface", kSurface);
	eAttr.addField("difference", kDifference);
	eAttr.setDefault(kSurface);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aNormalsMethod );mser;

	aBothNormals = nAttr.create("bothWays", "bws", MFnNumericData::kBoolean);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aBothNormals); mser;

	aNormalFalloff = nAttr.create("normalFalloff", "nmfo", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	nAttr.setDefault(1.0);
	
	st = addAttribute(aNormalFalloff); mser;	

	aNormal = nAttr.create("normalForce", "nrm", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aNormal); mser;	

	aDrag = nAttr.create("dragForce", "drg", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aDrag); mser;

	aRadialDrag = nAttr.create("radialDragForce", "rdrg", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aRadialDrag); mser;

	aEasyway = nAttr.create("easyForce", "efrc", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	nAttr.setDefault(0.0);
	st = addAttribute(aEasyway); mser;

	aNormalLookup = nAttr.create("normalLookup", "nmlk", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aNormalLookup); mser;	

	aDragLookup = nAttr.create("dragLookup", "drlk", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aDragLookup); mser;

	aRadialDragLookup = nAttr.create("radialDragLookup", "rdlk", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aRadialDragLookup); mser;

	aEasywayLookup = nAttr.create("easyLookup", "elk", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aEasywayLookup); mser;






	aOffset = nAttr.create("surfaceOffset", "sof", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aOffset); mser;

	aCollision = nAttr.create("collision", "cln", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aCollision); mser;

	aBounce = nAttr.create("bounce", "bnc", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aBounce); mser;

	aFriction = nAttr.create("friction", "frc", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	st = addAttribute(aFriction); mser;

	return( MS::kSuccess );
}


MStatus geometryField::compute(const MPlug& plug, MDataBlock& data)
{

	MStatus st;

	if( !(plug == mOutputForce) ) return( MS::kUnknownParameter );

	MVectorArray outForce;


	MString nodeName = MFnDependencyNode(thisMObject()).name();



	/////////////////////////	/////////////////////////

	int multiIndex = plug.logicalIndex( &st ); msert;	

	short int nodeState = data.inputValue( state).asShort();
	if (nodeState == 0)  { // normal

		if (st.error()) return( MS::kFailure);

		MArrayDataHandle hInputArray = data.outputArrayValue( mInputData, &st );msert;

		st = hInputArray.jumpToElement( multiIndex );msert;

		MDataHandle hCompound = hInputArray.inputValue( &st );mser;

		MDataHandle hPosition = hCompound.child( mInputPositions );
		MObject dPosition = hPosition.data();
		MFnVectorArrayData fnPosition( dPosition );
		MVectorArray points = fnPosition.array( &st );mser;

		MDataHandle hVelocity = hCompound.child( mInputVelocities );
		MObject dVelocity = hVelocity.data();
		MFnVectorArrayData fnVelocity( dVelocity );
		MVectorArray velocities = fnVelocity.array( &st );mser;

		MDataHandle hMass = hCompound.child( mInputMass );
		MObject dMass = hMass.data();
		MFnDoubleArrayData fnMass( dMass );
		MDoubleArray mass = fnMass.array( &st);mser

		double dt= 0.0;
		MDataHandle dtH = hCompound.child( mDeltaTime );	
		MTime dT = dtH.asTime();
		dt = dT.as( MTime::kSeconds );
		/////////////////////////	/////////////////////////


		unsigned len = points.length();
	//	cerr << nodeName << "points.length(); " << len << endl;
		outForce=MVectorArray(len, MVector::zero);


		st = calculateForce(data,points,velocities,mass,dt,outForce);
	}
	MArrayDataHandle hOutArray = data.outputArrayValue( mOutputForce, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st ); mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnVectorArrayData fnOutput;
	MObject dOutput = fnOutput.create(outForce , &st );mser;
	hOut.set( dOutput);
	data.setClean( plug );
	return( MS::kSuccess );
}



MStatus geometryField::calculateForce(
	MDataBlock &data,
	const MVectorArray &points,
	const MVectorArray &velocities,
	const MDoubleArray &masses,
	const double & dt,
	MVectorArray &forces
){
	MStatus st = MS::kSuccess;





	const double bigNum = 10e+37 ;
	// is there any geometry connected ?
	/////////////////////////////////////////////////////////////// 
	triKdTree *	tree = 0; 
	triTreeData * triTree = 0;
	//bool triTreeExists = 0 ;

	MDataHandle hTriTree = data.inputValue(aTriTree, &st);msert;
	MObject dTriTree = hTriTree.data();
	MFnPluginData fnTriTree( dTriTree, &st );
	if (!(st.error())) {
		triTree = (triTreeData*)fnTriTree.data(); // get a pointer to triTreeData
		if (triTree->size())  tree = triTree->tree();	
	}
	if (!tree) return MS::kUnknownParameter;
	///////////////////////////////////////////////////////////////
	//MString nodeName = MFnDependencyNode(thisMObject()).name();

	unsigned pl = points.length();
	unsigned vl = velocities.length();
	unsigned ml = masses.length();
	if (!(pl && (pl == vl) && (pl == ml)))  return MS::kUnknownParameter;


	double nrm	=	data.inputValue(aNormal).asDouble()	;
	double drag	=	data.inputValue(aDrag).asDouble()	;
	double rdrag	=	data.inputValue(aRadialDrag).asDouble()	;
	double easy	=	data.inputValue(aEasyway).asDouble()	;
	bool bothNorms	=	data.inputValue(aBothNormals).asBool();
	double atten	=	data.inputValue(mAttenuation).asDouble()	;
	double radius	=	data.inputValue(mMaxDistance).asDouble()	;
	double friction	=	data.inputValue(aFriction).asDouble()	;
	double offset	=	data.inputValue(aOffset).asDouble()	;
	double bounce	=	data.inputValue(aBounce).asDouble()	;
	double normalFalloff	=	data.inputValue(aNormalFalloff).asDouble()	;

	//bool useMax	=	data.inputValue(mUseMaxDistance).asBool();
	Sides sides = (Sides)data.inputValue(aSides).asShort();
	NormalsMethod nMmeth = (NormalsMethod)data.inputValue(aNormalsMethod).asShort();
	double collision	=	data.inputValue(aCollision).asDouble();

	bool doTop = ((sides == geometryField::kSidesBoth) || (sides == geometryField::kSidesTop) );
	bool doBottom = (sides == geometryField::kSidesBoth) || (sides == geometryField::kSidesBottom) ;


	if (!(nrm || drag || collision || rdrag || easy)) return MS::kUnknownParameter;


	MObject thisNode = thisMObject();
	lookup normalLookup, dragLookup, radialDragLookup, easywayLookup;
	st = getLookup(thisNode, aNormalLookup ,normalLookup);mser;
	st = getLookup(thisNode, aDragLookup ,dragLookup);mser;
	st = getLookup(thisNode, aRadialDragLookup ,radialDragLookup);mser;
	st = getLookup(thisNode, aEasywayLookup ,easywayLookup);mser;
	//////////////////////////////////////////////////////////
	


	// if (!useMax) {
	// 	radius = bigNum;
	// 	atten = 0.0;
	// }

	//bool useFalloff = 	useMax && (!geometryField::isFalloffCurveConstantOne(&st));mser;

	//forces.setLength(pl);
	if (sides == geometryField::kSidesOff) {
		for (unsigned i = 0; i<pl; i++) {
			forces.set(MVector::zero,i);
		}
		return( MS::kSuccess );
	}

	//nrm = nrm * mag;
	bool doNorm = (0.0 != nrm);
	bool doEasy = (0.0 != easy);
	//drag = drag * mag;
	bool doDrag = (0.0 != drag);
	bool doRadialDrag = (0.0 != rdrag);
	bool doCollision = (0.0 != collision);
	double fps = 0.0;

	if (0.0 != dt) fps = 1.0 / dt; // frames per second

//	cerr << "nodeName --------------------------" << nodeName << endl;
	//MVectorArray t2(3);

	MPoint c   ; // the center of the search sphere
	


	
	for (unsigned i = 0; i<pl; i++) {

		MVector & f = forces[i];
		const MVector &v = velocities[i];
		f=MVector::zero;
		c = MPoint(points[i]);
		double searchRad = radius;
		triData tb;

		tree->closestTri( tree->root(), c ,searchRad,  tb);

		if (searchRad < radius) {  // we found a point on a triangle

	//		cerr << "found: searchRad = " << searchRad << " -- radius " << radius <<  endl;
			

			MVector triN = tb.normal();
			MVector hit = tb.cachedPoint() + (triN * offset);

			MVector diff = c - hit;
			MVector diffN = diff.normal();


			double dot = diffN * triN;
			double absDot = ( dot < 0  ) ? -dot : dot;
			//cerr  << "DOT" << dot << endl;
			double mult;
			if ( normalFalloff == 0.0){ 
				mult = 1.0;
			} else if ( normalFalloff == 1.0){ 
				mult = absDot;
			} else {
				mult = pow(absDot, normalFalloff);
			}
			if (mult == 0.0) continue;

			


			MVector dv; // relative velocity
			MVector triV; // velocity at hit point
			if (doCollision || doDrag || doRadialDrag || doEasy ) {
				triV =  tb.calcVelocity(fps) ;
				dv = triV - v;
			}



			if (doCollision) {
				//cerr << "do collisions " << endl;
				// predict future state
				MVector p2 = c + (v * dt); // point in future
				MVector hp2 = hit + (triV * dt); // a point on the (offset) triangle in future
				//cerr << "c " << c << " --- hp2 " << hp2 << endl;
				//cerr << "p2 " << p2 << " --- triN " << triN << endl;
				//cerr << "v " << v << " --- dt " << dt << endl;

				//double dot2  = MVector(c - hp2) * triN;  


				double dotUnder  = MVector(p2 - hp2) * triN;  // will we be under the triangle in the future

				if (dotUnder < 0.0) { // we will be under, so get to the surface

					//cerr << "will be under " << endl;

					//MVector dp =  -dot2 * triN  ; // desired change in position
					MVector dp =  hp2 -c  ; // desired change in position

					//MVector dp =  hp2 -c  ; // desired change in position

					MVector v2 = dp * fps;
					MVector dv2 =  v2 - v;

					MVector dv2N = (dv2*triN) * triN; // amount we want velocity to change in the normal direction 


					MVector dv2T = (triN^dv2)^triN * friction;

					if (bounce) {
						dv2N = dv2N * (1.0+bounce);
					}

					// cerr << "dv2 " << dv2 << endl;
					MVector a = (dv2N + dv2T)  * fps;


					f += (a * masses[i]) * collision ;
					//f += a  ;
				}
			}
			//double dot = diffN * tb.normal();


			bool isTop = ( dot > 0  ) ;

			if ((isTop && doTop) || ((!isTop) && doBottom)) {

				double dist = diff.length() ;
				double normalizedDist = dist / radius;
				// double attenVal =  1.0;

				// if (useMax) {
					// attenVal =  mayaMath::attenuate(dist,radius,atten);

					// if (useFalloff) {

					// 	double normalizedDist  = dist / radius;
					// 	attenVal *= geometryField::falloffCurve(normalizedDist, &st); mser;

					// }
				// }

				if (doNorm) {
					// changed from using normals to using normalized difference
					
					double multVal = normalLookup.evaluate(normalizedDist) * nrm;

					// cerr << "normalizedDist: " << normalizedDist << endl;
					// cerr << "nrm: " << nrm << endl;
					// cerr << "multVal: " << multVal << endl;
					
					if (nMmeth == geometryField::kDifference) {
						if ( bothNorms ||  dot  > 0.0) {
							f += diffN *  multVal;
						} else {
							f += -(diffN * multVal);  
						}
					} else {
						MVector facingNorm = tb.normal();
						if (dot  < 0.0) facingNorm = -facingNorm;
						if ( bothNorms ||  dot  > 0.0) {
							f += facingNorm  * multVal;
						} else {
							f += -(facingNorm  * multVal);
						}
					}
				}

				if (doDrag) {
					double multVal = dragLookup.evaluate(normalizedDist) * drag;
						//MVector dv = tb.calcVelocity(fps) - v;  // relative velocity (in direction of drag force)
					f += (dv * multVal);
				}

				if (doRadialDrag) {
					double multVal = radialDragLookup.evaluate(normalizedDist) * rdrag;
					double dot = triV*triN;
					f+= triN * dot * multVal;

				}

				if (doEasy) {
					double multVal = radialDragLookup.evaluate(normalizedDist) * easy;
					f+= (((dv^triN)^triN).normal() * multVal);
				}
				
			}
			f *=mult;
		}
		
//		cerr << "force " << f << endl;
	}	
//	cerr << "end " << nodeName << " compute" << endl;
	return( MS::kSuccess );
}


MStatus geometryField::getForceAtPoint(const MVectorArray&	points,
	const MVectorArray&	velocities,
	const MDoubleArray&	masses,
	MVectorArray&	forceArray,
	double	deltaTime)
{
	MDataBlock data = forceCache();
	calculateForce(data, points, velocities, masses, deltaTime ,forceArray);
	return MS::kSuccess;
}

MStatus geometryField::iconSizeAndOrigin(	GLuint& width,
	GLuint& height,
	GLuint& xbo,
	GLuint& ybo   )
//
//	This method is not required to be overridden.  It should be overridden
//	if the plug-in has custom icon.
//
//	The width and height have to be a multiple of 32 on Windows and 16 on 
//	other platform.
//
//	Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//	(xbo, ybo) of (4,4) makes sure the icon is center at origin.
//
{
	width = 32;
	height = 32;
	xbo = 4;
	ybo = 4;
	return MS::kSuccess;
}

