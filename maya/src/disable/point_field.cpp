
/*
*  pointField.cpp
*  jtools
*
*  Created by Julian Mann on 31/12/2006.
*  Copyright 2006 hooly|mama. All rights reserved.
*
*/


#include <maya/MIOStream.h>
#include <math.h>


#include <maya/MTime.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>

#include <maya/MArrayDataHandle.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFnEnumAttribute.h>

#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnPluginData.h>
#include <maya/MArrayDataBuilder.h>


#include "attrUtils.h"
#include "ptData.h"
#include "ptKdTree.h"
#include "ptTreeData.h"

#include "point_field.h"

#include "jMayaIds.h"


MObject pointField::aRadialRange;
MObject pointField::aRadialRamp;
MObject pointField::aOrbitalRange;
MObject pointField::aOrbitalRamp;
MObject pointField::aDragRange;
MObject pointField::aDragRamp;
MObject pointField::aMaxNeighbours;
MObject pointField::aMaxDistancePP;

MObject pointField::aDoRadial;
MObject pointField::aDoOrbital;
MObject pointField::aDoDrag;


MObject pointField::aPointTree;

const double epsilon = 0.00000001;
const double bigNum = 10e+17;
MTypeId pointField::id( k_pointField );

pointField::pointField(){ 
};

pointField::~pointField() { 
};



void *pointField::creator()
{
    return new pointField;
}


MStatus pointField::initialize()
{
    MStatus st;

//cerr << "init" << endl;
    MFnTypedAttribute tAttr;
    MFnNumericAttribute nAttr;
    MFnEnumAttribute eAttr;

    aPointTree = tAttr.create("ptTreeNode", "ptre", ptTreeData::id ) ; mser;
    tAttr.setStorable(false);
    tAttr.setCached(false);
    st = addAttribute( aPointTree ); mser;
    addAttribute(aPointTree);

    aMaxDistancePP = tAttr.create("maxDistancePP", "mdpp", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setReadable(false);
    tAttr.setArray(true);  
    st = addAttribute(aMaxDistancePP);

    aMaxNeighbours = nAttr.create( "maxNeighbours", "mxn", MFnNumericData::kInt);
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
    nAttr.setDefault(5);
    st = addAttribute(aMaxNeighbours);mser;

    aRadialRange = nAttr.create("radialRange","rrg", MFnNumericData::k2Float);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    st=addAttribute(aRadialRange);

    aRadialRamp = MRampAttribute::createCurveRamp("radialRamp","rrmp");
    st = addAttribute( aRadialRamp );mser;

    aOrbitalRange = nAttr.create("orbitalRange","org", MFnNumericData::k2Float);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    st=addAttribute(aOrbitalRange);

    aOrbitalRamp = MRampAttribute::createCurveRamp("orbitalRamp","ormp");
    st = addAttribute( aOrbitalRamp );mser;

    aDragRange = nAttr.create("dragRange","drg", MFnNumericData::k2Float);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    st=addAttribute(aDragRange);

    aDragRamp = MRampAttribute::createCurveRamp("dragRamp","drmp");
    st = addAttribute( aDragRamp );mser;

    aDoRadial = nAttr.create( "doRadial", "drd", MFnNumericData::kBoolean);
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
    nAttr.setDefault(true);
    st = addAttribute(aDoRadial);mser;


    aDoOrbital = nAttr.create( "doOrbital", "dor", MFnNumericData::kBoolean);
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
    nAttr.setDefault(true);
    st = addAttribute(aDoOrbital);mser;

    aDoDrag = nAttr.create( "doDrag", "ddr", MFnNumericData::kBoolean);
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
    nAttr.setDefault(true);
    st = addAttribute(aDoDrag);mser;


    return( MS::kSuccess );
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
MStatus pointField::doRampLookup( 
    const MObject& attribute, 
    const MDoubleArray& in, 
    float min,float max, 
    MFloatArray& results ) const
{
    MStatus st;
    MRampAttribute rampAttr( thisMObject(), attribute, &st ); msert;
    float range = max - min;
    unsigned n = in.length();
    results.setLength(n);

    for ( unsigned i = 0; i < n; i++ )
    {
        float & value = results[i];
        rampAttr.getValueAtPosition( float(in[i]), value, &st ); mser;
        value = (value - min) * range;
    }

    return MS::kSuccess;
}

MStatus pointField::compute( const MPlug& plug, MDataBlock& data)
//
//  Descriptions:
//    compute output force.
{
    MStatus st;


//cerr << "in compute" << endl;
    if(!(plug == mOutputForce))return( MS::kUnknownParameter ); 
    // JPMDBG;

///////////////////////// /////////////////////////

    int multiIndex = plug.logicalIndex( &st ); mser;
    if (st.error()) return( MS::kFailure);

    MArrayDataHandle hInputArray = data.outputArrayValue( mInputData, &st );mser;
    st = hInputArray.jumpToElement( multiIndex );msert;
    MDataHandle hCompound = hInputArray.inputValue( &st );mser;

    MDataHandle hPosition = hCompound.child( mInputPositions );
    MObject dPosition = hPosition.data();
    MFnVectorArrayData fnPosition( dPosition );
    MVectorArray points = fnPosition.array( &st );mser;

    MDataHandle hVelocity = hCompound.child( mInputVelocities );
    MObject dVelocity = hVelocity.data();
    MFnVectorArrayData fnVelocity( dVelocity );
    MVectorArray velocities = fnVelocity.array( &st );mser;

    MDataHandle hMass = hCompound.child( mInputMass );
    MObject dMass = hMass.data();
    MFnDoubleArrayData fnMass( dMass );
    MDoubleArray masses = fnMass.array( &st);mser


    MDoubleArray distances;
    MArrayDataHandle hDistanceArray = data.outputArrayValue(aMaxDistancePP);
    st = hDistanceArray.jumpToElement( multiIndex );
    if (! st.error()) {
        MDataHandle hDistancePP = hDistanceArray.inputValue( &st );
        if (! st.error()) {
            MObject dDistancePP = hDistancePP.data();
            MFnDoubleArrayData fnDistancePP( dDistancePP );
            distances = fnDistancePP.array( &st);
            if (st.error()) {
                distances = MDoubleArray();
            }
        }
    }


    double dt= 0.0;
    MDataHandle dtH = hCompound.child( mDeltaTime );  
    MTime dT = dtH.asTime();
    dt = dT.as( MTime::kSeconds );
///////////////////////// /////////////////////////

    MVectorArray forceArray;
    // JPMDBG;
   calcForce( data,  points,   velocities, masses,distances,  forceArray );
    // JPMDBG;

// get output data handle
    MArrayDataHandle hOutArray = data.outputArrayValue( mOutputForce, &st);mser;
    MArrayDataBuilder bOutArray = hOutArray.builder( &st );mser;
    MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;

    MFnVectorArrayData fnOutputForce;
    MObject dOutputForce = fnOutputForce.create( forceArray, &st );mser;
    hOut.set( dOutputForce );
    data.setClean( plug );
    // JPMDBG;

    return( MS::kSuccess );
}


MStatus pointField::calcForce(
    MDataBlock &data,       
    const MVectorArray &points,   
    const MVectorArray &velocities, 
    const MDoubleArray &masses,   
    const MDoubleArray &distances,   
    MVectorArray &outputForce   
    )
{

    MStatus st;

    unsigned int pl = points.length() ;
    outputForce= MVectorArray(pl);
    if( pl  != velocities.length() ) return MS::kSuccess;

    ptTreeData * ptd = 0;
    MDataHandle h = data.inputValue(aPointTree, &st);msert;
    MObject d = h.data();
    MFnPluginData plfn( d , &st); msert;
    ptd =  (ptTreeData*)plfn.data();
//cerr << "h2" << endl;

    if (!(ptd->size()))  {
        return MS::kSuccess;
    }
    ptKdTree * pkd = ptd->tree();


// get scalars
/////////////////////////////////////////////////////
    double magnitude = data.inputValue(mMagnitude).asDouble();

    bool doRadial = data.inputValue(aDoRadial).asBool();
    bool doOrbital = data.inputValue(aDoOrbital).asBool();
    bool doDrag = data.inputValue(aDoDrag).asBool();



    const float2 & radialRangeVals = data.inputValue( aRadialRange ).asFloat2();
    const float2 & orbitalRangeVals = data.inputValue( aOrbitalRange ).asFloat2();
    const float2 & dragRangeVals = data.inputValue( aDragRange ).asFloat2();
    unsigned maxNeighbours = data.inputValue(aMaxNeighbours).asInt();

    if (! maxNeighbours) {  return MS::kSuccess;}


/////////////////////////////////////////////////////
    float  radialRange = radialRangeVals[1] - radialRangeVals[0];
    float  orbitalRange = orbitalRangeVals[1] - orbitalRangeVals[0];
    float  dragRange = dragRangeVals[1] - dragRangeVals[0];

    if (fabs(radialRange) < epsilon ) doRadial = false;
    if (fabs(orbitalRange) < epsilon ) doOrbital = false;
    if (fabs(dragRange) < epsilon ) doDrag = false;

    if (! (doRadial || doOrbital || doDrag)) {  return MS::kSuccess; }

    // outputForce.clear();

    double  maxDist = data.inputValue( mMaxDistance ).asDouble();

    if (maxDist <= 0 ) {  return MS::kSuccess; }
    // cerr << "maxDist" << maxDist << endl;
    bool doDistancePP = (distances.length() == pl);
    MVector  resultVec;

    MObject thisObj = thisMObject();

    MRampAttribute radialRamp( thisObj , aRadialRamp, &st ); mser;
    MRampAttribute orbitalRamp( thisObj , aOrbitalRamp, &st ); mser;
    MRampAttribute dragRamp( thisObj , aDragRamp, &st ); mser;

    float radius = float(maxDist);

    float normDist, radialStrength, orbitalStrength, dragStrength;

    for (unsigned i = 0; i < pl; i ++ ) { 
        if (doDistancePP) {
            radius = float(maxDist * distances[i]) ;
        }
        const float & cradius =  radius;
     // cerr << "cradius" << cradius << endl;

        resultVec = MVector::zero;

        KNN_QUEUE q ;
        for (unsigned j = 0;j<maxNeighbours;j++) {
            knnSearchData k;
            k.dist = cradius;
            k.point=MVector::zero;
            q.push(k);
        }

        const MVector & point = points[i];
        const MVector & velocity = velocities[i];
        
        pkd->closestNPts(pkd->root(), point, q);


        MVector cross, diffN;
        while (!q.empty()) {
            if (q.top().dist < cradius) {
                const MVector & foundPoint = q.top().point;
                
                MVector diff = (point - foundPoint  );
                if (diff.isEquivalent(MVector::zero) ) continue;
                diffN = diff.normal();
                normDist = float(q.top().dist) / cradius;

                // RADIAL: give a push away from the found  point.
                //////////////////////////////////
                if (doRadial) {
                    radialRamp.getValueAtPosition( normDist,radialStrength, &st ); mser;
                    radialStrength = radialRangeVals[0] + (radialStrength  * radialRange);
                    // cerr << "radialRangeVals[0]: " << radialRangeVals[0] << endl;
                    // cerr << "radialRange: " << radialRange << endl;
                    // cerr << "radialStrength: " << radialStrength << endl;
                    resultVec += diffN * radialStrength;
                }
                //////////////////////////////////


                // ORBITAL: give a push around the point.
                // specifically, a vector perpendicular to diffN vector
                // and in the plane defined by it and the velocity of
                // our search point. May want to adjust this behaviour if
                // we dont get the desired swirly effects
                //////////////////////////////////
                if (doOrbital){
                    orbitalRamp.getValueAtPosition( normDist,orbitalStrength, &st ); mser;

                    orbitalStrength = orbitalRangeVals[0] + (orbitalStrength  * orbitalRange);
                    cross = velocity.normal() ^ diffN;
                    if (! cross.isEquivalent(MVector::zero)) {
                        cross = diffN ^ cross;
                        if (! cross.isEquivalent(MVector::zero)) {
                            resultVec += cross * orbitalStrength;
                        }
                    }

                }
                //////////////////////////////////

                // DRAG: impart some relative velocity
                //////////////////////////////////
                if (doDrag) {
                    dragRamp.getValueAtPosition( normDist,dragStrength, &st ); mser;
                    dragStrength = dragRangeVals[0] + (dragStrength  * dragRange);
                    resultVec += (q.top().velocity - velocity) * dragStrength;
                }
                //////////////////////////////////



            }
            q.pop();
        }

        outputForce.set((resultVec * magnitude), i); 
    }


    return MS::kSuccess;
}



MStatus pointField::getForceAtPoint(const MVectorArray& points,
    const MVectorArray& velocities,
    const MDoubleArray& masses,
    MVectorArray& forceArray,
    double  deltaTime)
//
//    This method is not required to be overridden, it is only necessary
//    for compatibility with the MFnField function set.
//
{
    MStatus ReturnStatus;

    MDataBlock data = forceCache();
    MDoubleArray distances;
    calcForce(data, 
        points, 
        velocities, 
        masses, 
        distances,
        forceArray);

    return MS::kSuccess;
}


MStatus pointField::iconSizeAndOrigin(  GLuint& width,
    GLuint& height,
    GLuint& xbo,
    GLuint& ybo   )
//
//  This method is not required to be overridden.  It should be overridden
//  if the plug-in has custom icon.
//
//  The width and height have to be a multiple of 32 on Windows and 16 on 
//  other platform.
//
//  Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//  (xbo, ybo) of (4,4) makes sure the icon is center at origin.
//
{
    width = 32;
    height = 32;
    xbo = 4;
    ybo = 4;
    return MS::kSuccess;
}

