
#ifndef _POINT_FIELD_H
#define _POINT_FIELD_H
/*
 *  pointField.h
 *  jtools
 *
 *  Created by Julian Mann on 31/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */


#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxFieldNode.h>
#include <maya/MFloatArray.h>
 
#include "errorMacros.h"

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif


class pointField: public MPxFieldNode
{
public:
  pointField() ;
  virtual ~pointField() ;
  
  static void   *creator();
  static MStatus  initialize();
  
  // will compute output force.
  //
  virtual MStatus compute( const MPlug& plug, MDataBlock& data );
  
    ///
    virtual MStatus getForceAtPoint(const MVectorArray& point,
                  const MVectorArray& velocity,
                  const MDoubleArray& mass,
                  MVectorArray& force,
                  double deltaTime);
                  
  virtual MStatus iconSizeAndOrigin(  GLuint& width,
            GLuint& height,
            GLuint& xbo,
            GLuint& ybo   );
  

  // NOTE otbital means orthogonal to some given vector
  // usually the particle's velocity



  static MObject aDoRadial;
  static MObject aDoOrbital;
  static MObject aDoDrag;

  static MObject aRadialRange;
  static MObject aRadialRamp;
  static MObject aOrbitalRange;
  static MObject aOrbitalRamp;
  static MObject aDragRange;
  static MObject aDragRamp;
  static MObject aMaxNeighbours;
  static MObject aMaxDistancePP;
  static MObject aPointTree;

  static MTypeId  id;
  
private:

  // methods to compute output force.
  MStatus calcForce( 
               MDataBlock &data,        
               const MVectorArray &points,    
               const MVectorArray &velocities,  
               const MDoubleArray &masses,   
               const MDoubleArray &distances, 
               MVectorArray &outputForce );
  

  MStatus doRampLookup( 
    const MObject& attribute, 
    const MDoubleArray& in, 
    float min,float max, 
    MFloatArray& results ) const;
};


#endif




















