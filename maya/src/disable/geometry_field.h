
#ifndef _GEOMETRY_FIELD_H
#define _GEOMETRY_FIELD_H


#include <maya/MPxFieldNode.h>
#include <maya/MTypeId.h> 

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include "lookup.h"

class geometryField : public MPxFieldNode
{
public:
	geometryField();
	virtual				~geometryField(); 
	
	virtual MStatus compute( const MPlug& plug, MDataBlock& data );
   	virtual MStatus getForceAtPoint(  const MVectorArray& point,
									const MVectorArray& velocity,
									const MDoubleArray& mass,
									MVectorArray& force,
									double deltaTime
									);
									
	virtual MStatus iconSizeAndOrigin(	GLuint& width,
						GLuint& height,
						GLuint& xbo,
						GLuint& ybo   );
						
	MStatus getLookup(const MObject &object, MObject &attr, lookup & result);
						
	
	static void*		creator();
	static MStatus		initialize();
	static MTypeId		id;
	
	static MObject aTriTree;  // triangles tree

	static MObject		aNormalFalloff;	
	static MObject		aNormalsMethod;				
	static MObject		aBothNormals;				
	static MObject		aSides;
				
	static MObject		aNormal;				
	static MObject		aDrag;		
	static MObject		aRadialDrag;	
	static MObject		aEasyway;	
		

	static MObject		aNormalLookup;				
	static MObject		aDragLookup;		
	static MObject		aRadialDragLookup;	
	static MObject		aEasywayLookup;	


	static MObject		aCollision;
	
	static MObject		aOffset;		
	static MObject		aBounce;
	static MObject		aFriction;
	

	
private:
	
	enum Sides { kSidesOff, kSidesTop, kSidesBottom, kSidesBoth };
	enum NormalsMethod { kSurface, kDifference};

		MStatus calculateForce(
							   MDataBlock &data,
							   const MVectorArray &points,
							   const MVectorArray &velocities,
							   const MDoubleArray &masses,
							   const double &dt,
							   MVectorArray &forces
							   );

};


#endif




















