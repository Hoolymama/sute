import pymel.core as pm

import os
import glob
 

MAYA_PARENT_WINDOW = "MayaWindow"
HOOLY_MENU = "HoolyMenu"

def ensure_hooly_menu():
    menu = next((m for m in pm.lsUI(menus=True) if m.getLabel() == "Hooly"), None)
    if menu:
        return menu
    return pm.menu(HOOLY_MENU, label="Hooly", tearOff=True, parent=MAYA_PARENT_WINDOW)
 

class SuteMenu(object):
    def __init__(self):
        self.hooly_menu = ensure_hooly_menu()
        pm.setParent(self.hooly_menu, menu=True)
        
        pm.menuItem(label="Sute", subMenu=True)

        pm.menuItem(
        label="Create Tether Field",
        command=pm.Callback(create_tether_field)
    )

def create_tether_field():
    p = pm.ls(sl=True, dag=True, leaf=True, type="nParticle")[0]
    t = pm.createNode("tetherField")
    pm.connectDynamic(p,f=t)





