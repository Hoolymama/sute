---
## title, author, email, creation time, and description will be set automatically at compile time. 
## Uncomment the lines below only if you want to override these fields.
# title: Alternative title
# author: Someone Else
# email: someone.else@example.com
# created_at: 30 April 2012
# description: Learn how to use this tool.
---

h4. <%= item[:product_name] %> is a collection of Mmaya field nodes.

The word *<%= item[:product_name] %>* is a collective noun for bloodhounds.

<%= item[:product_name] %> provides:

* geometry_field - A field that applies force based on proximity to some geometry.
* point_field - A field that applies force based on proximity to an array of points.